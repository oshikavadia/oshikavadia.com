/**
 * Created by Oshi on 15-Dec-17.
 */
var canvas, ctx;
document.addEventListener("DOMContentLoaded", function () {
    canvas = document.getElementById("bgCanvas");
    ctx = canvas.getContext("2d");
    renderCanvas();
});


window.addEventListener('resize', function () {
    renderCanvas();
});

function renderCanvas() {
    var width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;
    var height = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;
    canvas.width = width;
    canvas.height = height;
    var trunkHeight = height * 0.25;
    if (/Mobi/.test(navigator.userAgent)) {
        trunkHeight *= 0.75;
    }
    drawTree(canvas.width / 2, canvas.height, trunkHeight, 0);
}

function drawTree(startX, startY, len, angle) {
    ctx.beginPath();
    ctx.save();

    ctx.translate(startX, startY);
    ctx.rotate(angle * Math.PI / 180);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, -len);
    ctx.stroke();

    if (len < 10) {
        ctx.restore();
        return;
    }
    var rand = Math.random() * (40 - 10) + 10;
    drawTree(0, -len, len * 0.8, -rand);
    drawTree(0, -len, len * 0.8, rand);

    ctx.restore();
}